let FAClient = null;
let recordId = null;
let linesGlobal = null;
let productList = null;

let isSalesOrder = null;

let salesOrderList = [];
let salesOrderListFiltered = [];

let keyGlobal = "";

let executingRequest = false;

// aHR0cDovL2xvY2FsaG9zdDo1MDAw localhost:5000
//aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3NvcmVsZXNlcy1wcm9kdWN0cy1tYXhsaXRl // production https://fa-solutions.gitlab.io/soreleses-products-maxlite
////aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3Byb2R1Y3RzLWxpc3QtbWF4bGl0ZQ==  // production https://fa-solutions.gitlab.io/products-maxlite

const SERVICE = {
    name: 'FreeAgentService',
    appletId: `aHR0cDovL2xvY2FsaG9zdDo1MDAw`,
};

let config = {
    poLines: {
        name: "po_release",
        fields: {
            soRecordRef: 'po_release_field1'
        }
    },
    lines: {
        name: 'quote_item',
        fields: {
            itemNumber : 'quote_item_field0',
            itemDescription : 'quote_item_field2',
            descriptionSpecial : 'description',
            itemType : 'quote_item_field7',
            competitorName : 'quote_item_field8',
            aPrice : 'quote_item_field9',
            hotPrice : 'quote_item_field10',
            price : 'quote_item_field11',
            lineAmount : 'quote_item_field12',
            commissionAmount : 'quote_item_field13',
            quantity : 'quote_item_field14',
            commissionPercent : 'quote_item_field17',
            commissionPercentOverwrite : 'quote_item_field18',
            lifeCycle : 'quote_item_field16',
            itemCategory : 'quote_item_field19',
            available : 'quote_item_field20',
            wareHouse : 'quote_item_field21',
            productName : 'quote_item_field22',
            alternativeTo : 'quote_item_field23',
            typeText: 'quote_item_field24',
            releaseAmount: 'quote_item_field25'
        }
    },
    products: {
        name: 'product',
        fields: {
            descriptionName: 'product_field1',
            description: 'description',
            itemNumber: 'product_field0',
            aPrice: 'product_field2',
            hotPrice: 'product_field3',
            orderCode: 'product_field4',
            commissionPercent: 'product_field5',
            active: 'product_field6',
            mainCategory: 'product_field7',
            subCategory: 'product_field8',
            lifeCycle: 'product_field9',
            onHand: 'product_field11',
            available: 'product_field12',
            allocated: 'product_field13'
        }
    },
    parent : {
        name: 'quote'
    }
}


function startupService() {
    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    let searchEl = document.getElementById('search-input');
    let searchSelectEl = document.getElementById('search-select');
    let submitSo = document.getElementById('submit-so-button');
    let reorderButton = document.getElementById('reorder-lines-button');

    reorderButton.addEventListener('click', (e) => {
        console.log(linesGlobal);
        renderedReorderLines(linesGlobal);
    });


    keyGlobal = FAClient.params.key;

    FAClient.on("openQuote", ({quote, opportunity}) => {
        let quoteId = quote?.id;
        if(quoteId) {
            console.log(quote.id);
            FAClient.navigateTo(`/quote/view/${quoteId}`);
            FAClient.open();
            openProducts(quote, searchSelectEl)
        }
    })

    FAClient.on("openProducts", ({record}) => {
        openProducts(record, searchSelectEl)
    });


    FAClient.on("openSO", ({record}) => {
        isSalesOrder = true;
        recordId = record.id;
        searchSelectEl.innerHTML = '<option>SO Release</option>';
        generateList({products: []})
        document.getElementById('submit-so-button').style.display = "flex";
        FAClient.listEntityValues(
            {
                entity: config.lines.name,
                filters: [
                    {
                        field_name: "parent_entity_reference_id",
                        operator: "includes",
                        values: [record.id],
                    },
                ],
            },
            (lines) => {
                linesGlobal = lines;
                FAClient.open();
                FAClient.listEntityValues({entity: config.products.name}, (data) => {
                    productList = getFilteredProductList(data, linesGlobal, 'Included');
                    salesOrderListFiltered = productList;
                    generateList({products: salesOrderListFiltered})
                })
            });

    })
    searchEl.addEventListener("keyup", (event) => {
        generateList({isChange: true})
    });
    searchSelectEl.addEventListener("change", (event) => {
        generateList({isChange: true})
    });
    submitSo.addEventListener('click', (event) => {
        createSo();
    });
}

function openProducts(record, searchSelectEl) {
    generateList({products: []})
    searchSelectEl.innerHTML = '';
    let selectOptions = ['All', 'Included'];
    selectOptions.map(subCat => {
        let subOption = document.createElement('option');
        subOption.innerText = subCat;
        searchSelectEl.appendChild(subOption);
    })
    isSalesOrder = null;
    document.getElementById('submit-so-button').style.display = "none";
    let list = document.getElementById('order-list');
    list.innerHTML = '';
    recordId = record.id;
    FAClient.listEntityValues(
        {
            entity: config.lines.name,
            filters: [
                {
                    field_name: "parent_entity_reference_id",
                    operator: "includes",
                    values: [record.id],
                },
            ],
        },
        (lines) => {
            linesGlobal = lines;
            FAClient.open();
            FAClient.listEntityValues({entity: config.products.name, limit: 10000}, (data) => {
                productList = data;
                generateList({products: productList})
            })
        });
}


function generateList({products=productList, lines=linesGlobal, isChange = false}) {
    let list = document.getElementById('order-list');
    let searchEl = document.getElementById('search-input');
    let searchSelectEl = document.getElementById('search-select');
    let searchText = searchEl.value && searchEl.value.trim() !== '' ? searchEl.value : null;
    let selectText = searchSelectEl?.value;
    let productsToDisplay = null;
    if (selectText && selectText === 'Included') {
        document.getElementById('reorder-lines-button').style.display = "flex";
    } else {
        document.getElementById('reorder-lines-button').style.display = "none";
    }
    if(products && selectText === 'All') {
        productsToDisplay = products;
    }
    if(isSalesOrder) {
        productsToDisplay = products;
    } else {
        if(products && selectText !== 'All') {
            productsToDisplay = getFilteredProductList(productList, linesGlobal, selectText);
        }
    }

    if (searchText) {
        productsToDisplay = searchList(productsToDisplay, searchText)
    }

    list.innerHTML = '';
    let subCategories = {};

    productsToDisplay?.map((product, index) => {
        let lineMatch = lines?.find(line => {
            return line.field_values[config.lines.fields.itemNumber].value === product.id;
        })

        let lineId = lineMatch?.id;
        let qty = lineMatch?.field_values[config.lines.fields.quantity]?.value || '';
        let price = lineMatch?.field_values[config.lines.fields.price]?.value || '';
        let co = lineMatch?.field_values[config.lines.fields.commissionPercentOverwrite]?.value || '';
        let whLine = lineMatch?.field_values[config.lines.fields.wareHouse]?.value || '';
        let itemType = lineMatch?.field_values[config.lines.fields.itemType]?.value || true;
        let alternateToLine =  lineMatch?.field_values[config.lines.fields.alternativeTo]?.value || null;
        let typeText =  lineMatch?.field_values[config.lines.fields.typeText]?.value || '';
        let specialDescriptionLine = lineMatch?.field_values[config.lines.fields.descriptionSpecial]?.value || null;
        let competitorName = lineMatch?.field_values[config.lines.fields.competitorName]?.display_value || '';

        let fieldValues = product.field_values;

        let productName = fieldValues[config.products.fields.descriptionName].display_value;
        let itemNumber = fieldValues[config.products.fields.itemNumber].display_value;
        let productDescription = fieldValues[config.products.fields.description].display_value;
        let mainCategory = fieldValues[config.products.fields.mainCategory].display_value;
        let subCategory = fieldValues[config.products.fields.subCategory].display_value;
        let commissionPercent = fieldValues[config.products.fields.commissionPercent].formatted_value;

        subCategories[subCategory] = fieldValues[config.products.fields.mainCategory].display_value;

        let liEl = document.createElement('li');
        liEl.setAttribute('id', `liEl${index}`);
        liEl.setAttribute('data-id', `${product.id}`);
        liEl.setAttribute('data-index', `${index}`);
        if(lineId) {
            liEl.setAttribute('data-lineid', `${lineId}`);
        }
        if(qty > 0) {
            liEl.classList.add('included-in-lines')
        }

        let hasItems = qty && qty > 0 && price && price > 0;
        let productInfoContainer = document.createElement('div');

        let addButtonHtml = `<div id="addButton${index}" class="update-line-button">
                                         <button disabled>Add</button>
                               </div>`
        let updateButtonHtml = `<div id="updateButton${index}" class="update-line-button">
                                    <button>Update</button>
                                </div>`

        let removeButtonHtml = `<div style="display: ${hasItems ? 'flex' : 'none'}" id="removeButton${index}" class="update-line-button">
                                         <button>Remove</button>
                                 </div>`;
        let checkBox = `<div id="addButton${index}" className="update-line-button">
            <input id="checkbox${index}" type="checkbox" checked />
        </div>`

        //let renderedButton = `${hasItems && !isSalesOrder ? updateButtonHtml : addButtonHtml}`;


        function renderedButton() {
            let toRender = '';
            if(isSalesOrder) {
                toRender = `<div id="addButton${index}" className="update-line-button">
                                 <input id="checkbox${index}" type="checkbox" checked />
                             </div>`;
            } else {
                if(hasItems) {
                    toRender = `<div id="updateButton${index}" class="update-line-button">
                                    <button>Update</button>
                                </div>
                                <div style="display: ${hasItems ? 'flex' : 'none'}" id="removeButton${index}" class="update-line-button">
                                         <button>Remove</button>
                                 </div>`
                } else {
                    toRender = `<div id="addButton${index}" class="update-line-button">
                                         <button disabled="true">Add</button>
                               </div>
                                <div style="display: none" id="removeButton${index}" class="update-line-button">
                                         <button>Remove</button>
                                 </div>`
                }
            }
            return toRender;
        }



        // let disableProduct = ${ !activeProduct || !available || (available && available < 0) ? 'disabled' : ''}
        productInfoContainer.innerHTML = `<div>
                    <div class="row header-paragraph">
                         <div class="row">
                            <button id="collapseButton${index}"
                            class="dropdown-toggle" 
                            style="color: white; font-size: 13px; background: transparent; border: none" type="button" data-bs-toggle="collapse" data-bs-target="#multiCollapseExample${index}" aria-expanded="false" aria-controls="multiCollapseExample${index}">
                                <b style="white-space: pre-line"><span style="color: rgba(215, 217, 219, 1.00)"> (${itemNumber})</span> ${productName}</b>
                            </button>
                         </div>
                    </div>
                    <div class="collapse multi-collapse info-container" id="multiCollapseExample${index}">
                        <div class="info-container-description-row">
                            <textarea placeholder="Special Description" rows="2" cols="20" wrap="soft" class="description-text" id="descroiptionText${index}" value="${specialDescriptionLine || productDescription || ''}" data-prev="${productDescription}" name="${fieldValues[config.products.fields.descriptionName].value}">${specialDescriptionLine || productDescription || ''}</textarea>
                        </div>
                        <div class="info-container-row">
                           <div class="info-container_column">
                                      <div class="info-card-container">
                                       <div>
                                            <span class="input-label">Hot-Price:</span>
                                            <input class="input-select-fields" id="hotPrice${index}" type="text" disabled  />
                                        </div>
                                        
                                        <div>
                                             <span id="aPriceLabel${index}" class="input-label">Price (A-Price: )</span>
                                            <input class="input-select-fields" placeholder="Price" class="price" id="price${index}" type="number" min="0" value="${price}" data-prev="${price}" name="${fieldValues[config.products.fields.descriptionName].value}">
                                        </div>
                                        <div>
                                            <span class="input-label">Quantity</span>
                                            <input class="input-select-fields" placeholder="Quantity" class="quantity" id="qty${index}" type="number" min="0" value="${qty}" data-prev="${qty}" name="${fieldValues[config.products.fields.descriptionName].value}">
                                        </div>
                                         <div>
                                            <span id="coLabel${index}" class="input-label">CO %</span>  
                                            <input class="input-select-fields" placeholder="Commision Overwrite" class="commision" id="co${index}" type="number" value="${co}" min="0"">
                                        </div>
                                      </div>
                              </div> 
                           
                                 <div class="info-container_column">
                                      <div class="info-card-container">
                                        <div>
                                            <span class="input-label">LifeCycle:</span>  
                                            <input class="input-select-fields" id="lifeCycle${index}" type="text" disabled value="" />
                                        </div>
                                       <div>
                                            <span class="input-label">Type</span>   
                                            <input class="input-select-fields" placeholder="Enter Type" id="typeText${index}" type="text" value="${typeText}" data-prev="${typeText}" name="${fieldValues[config.products.fields.descriptionName].value}" />
                                        </div>
                                        <div>
                                            <span class="input-label">Competitor</span>  
                                            <select class="input-select-fields select-field-custom" id="competitor${index}"  style="width: 100%; height: 100%; border: none; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUiIHdpZHRoPSI3IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0yLjgyOCA0LjM2NEwwIDEuNTM2LjcwNy44MjhsMi44MjkgMi44MjlMNi4zNjQuODI4bC43MDcuNzA4TDMuNTM2IDUuMDd6IiBmaWxsPSIjQkJCRUJGIi8+PC9zdmc+&quot;); background-position: right 10px center; background-repeat: no-repeat; border-radius: 2px; background-color: white;"  name="${fieldValues[config.products.fields.descriptionName].value}_type">
                                              <option value="" ${competitorName === '' || !competitorName ? 'selected' : ''}>None</option>
                                              <option value="TCP" ${competitorName === 'TCP' ? 'selected' : ''}>TCP</option>
                                              <option value="Copper" ${competitorName === 'Copper' ? 'selected' : ''}>Copper</option>
                                            </select>
                                        </div>
                                         <div>
                                            <span class="input-label">Alternative Type</span>  
                                            <select class="input-select-fields select-field-custom" id="itemType${index}" data-prev="Primary" style="width: 100%; height: 100%; border: none; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUiIHdpZHRoPSI3IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0yLjgyOCA0LjM2NEwwIDEuNTM2LjcwNy44MjhsMi44MjkgMi44MjlMNi4zNjQuODI4bC43MDcuNzA4TDMuNTM2IDUuMDd6IiBmaWxsPSIjQkJCRUJGIi8+PC9zdmc+&quot;); background-position: right 10px center; background-repeat: no-repeat; border-radius: 2px; background-color: white;"  name="${fieldValues[config.products.fields.descriptionName].value}_type">
                                              <option value="Primary" ${itemType ? 'selected' : ''}>Primary</option>
                                              <option value="Alternative" ${!itemType ? 'selected' : ''}>Alternative</option>
                                            </select>
                                        </div>
                                      </div>
                                  </div>
                                   <div class="info-container_column">
                                      <div class="info-card-container">
                                        <div>
                                            <span class="input-label">WH:</span>  
                                            <select class="input-select-fields select-field-custom" id="wh${index}" value="${whLine || ''}" data-prev="" style="width: 100%; height: 100%; border: none; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUiIHdpZHRoPSI3IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0yLjgyOCA0LjM2NEwwIDEuNTM2LjcwNy44MjhsMi44MjkgMi44MjlMNi4zNjQuODI4bC43MDcuNzA4TDMuNTM2IDUuMDd6IiBmaWxsPSIjQkJCRUJGIi8+PC9zdmc+&quot;); background-position: right 10px center; background-repeat: no-repeat; border-radius: 2px; background-color: white;"  name="${fieldValues[config.products.fields.descriptionName].value}_type">
                                            </select>
                                        </div>
                                        <div>
                                            <span class="input-label">Available:</span>
                                            <input class="input-select-fields" id="available${index}" type="text" disabled value="" />
                                        </div>
                                        <div>
                                            <span class="input-label">Back Order:</span>  
                                            <input class="input-select-fields" id="backOrder${index}" type="text" disabled value="" />
                                        </div>
                                        <div>
                                            <span class="input-label">In Transit:</span>  
                                            <input class="input-select-fields" id="inTransit${index}" type="text" disabled value="" />
                                        </div>
                                      </div>
                                  </div> 
                                   <div class="info-container_buttons_column">
                                     ${renderedButton()}
                                  </div>
                            </div>  
                        <div class="alternate-selection-container-row" style="display: ${itemType ? 'none' : 'flex'}">
                                <span>Alternative to: </span>
                                <select class="alternate-select"></select>
                        </div>
            </div>`;

        let qtyInput = productInfoContainer.querySelector(`#qty${index}`);
        let priceInput = productInfoContainer.querySelector(`#price${index}`);
        let commissionInput = productInfoContainer.querySelector(`#co${index}`);
        let commissionLabel = productInfoContainer.querySelector(`#coLabel${index}`);
        let lifeCycleInput = productInfoContainer.querySelector(`#lifeCycle${index}`);
        let hotPriceInput = productInfoContainer.querySelector(`#hotPrice${index}`);
        let aPriceLabel = productInfoContainer.querySelector(`#aPriceLabel${index}`);
        let collapseButton = productInfoContainer.querySelector(`#collapseButton${index}`);
        let whField = productInfoContainer.querySelector(`#wh${index}`);
        let availableField = productInfoContainer.querySelector(`#available${index}`);
        let backOrderField = productInfoContainer.querySelector(`#backOrder${index}`);
        let inTransitField = productInfoContainer.querySelector(`#inTransit${index}`);
        let typeField = productInfoContainer.querySelector(`#itemType${index}`);
        let alternateSelection = productInfoContainer.querySelector(`.alternate-select`);
        let alternateSelectionContainer = productInfoContainer.querySelector(`.alternate-selection-container-row`);
        let addButton = productInfoContainer.querySelector(`#addButton${index} > button`);
        let updateButton = productInfoContainer.querySelector(`#updateButton${index} > button`);
        let removeButton = productInfoContainer.querySelector(`#removeButton${index} > button`);
        let checkboxEl = productInfoContainer.querySelector(`#checkbox${index}`);

        typeField.addEventListener('change', (event) => {
            console.log(event.currentTarget.value);
            alternateSelectionContainer.style.display = 'flex';
            alternateSelection.innerHTML = '';
            if(event.currentTarget.value === 'Alternative' && linesGlobal) {
                addAlternateToLines(alternateSelection, product, alternateToLine);
            } else {
                alternateSelectionContainer.style.display = 'none';
            }
        })

        qtyInput.addEventListener("change", async (event) => {
            let unitPrice = priceInput.valueAsNumber;
            let qty = qtyInput.valueAsNumber;
            if(!executingRequest) {
                executingRequest = true;
                try {
                    let commissionResponse = await getCommission(itemNumber, unitPrice, qty, 122).catch(e => console.error(e));
                    executingRequest = false;
                    if(commissionResponse) {
                        commissionInput.value = commissionResponse.toFixed(2);
                        if (addButton) {
                            addButton.disabled = false;
                        }
                    }
                } catch (e) {
                    console.error(e);
                    executingRequest = false;
                }
            }

        })

        priceInput.addEventListener("change", (event) => {
            //upsertLines(event, productInfoContainer, 'Price', productDescription,product, index)
        })

        if(isSalesOrder) {
            checkboxEl?.addEventListener("change", (event) => {
                if(event.target.checked) {
                    console.log(event)
                    liEl.classList.add('included-in-lines')
                    let found = salesOrderListFiltered.find(so => so.id === product.id);
                    if (!found) {
                        salesOrderListFiltered.push(productList.find(pro => pro.id === product.id))
                    }
                    console.log(salesOrderListFiltered);
                } else {
                    salesOrderListFiltered = salesOrderListFiltered.filter(so => so.id !== product.id);
                    console.log(salesOrderListFiltered);
                    liEl.classList.remove('included-in-lines')
                    //generateList({products: salesOrderListFiltered})
                }
            })
        } else {
            addButton?.addEventListener("click", (event) => {
                processLines(productInfoContainer, productDescription, product, index, 'add')
            })
            updateButton?.addEventListener("click", (event) => {
                processLines(productInfoContainer, productDescription, product, index, 'update')
            })
            removeButton?.addEventListener("click", (event) => {
                processLines(productInfoContainer, productDescription, product, index, 'remove')
            })
        }


        collapseButton.addEventListener('click', async (event) => {
            if(event.currentTarget && !event.currentTarget.classList.contains('collapsed')) {
                if (!itemType) {
                    console.log({itemType})
                    addAlternateToLines(alternateSelection, product, alternateToLine);
                }
                let itemResponse = await getItem(itemNumber);
                if (itemResponse) {
                    let {inventory, description, lifeCycle, prices} = await getItem(itemNumber);
                    let [A, HOT] = prices;
                    lifeCycleInput.value = lifeCycle;
                    hotPriceInput.value = HOT.price;
                    aPriceLabel.innerText = `Price (A-Price: ${A.price})`;
                    if (inventory) {
                        let availableTotal = 0;
                        let backOrderTotal = 0;
                        let inTransitTotal = 0;
                        inventory.map(({available, backOrder, inTransit, warehouseCd}) => {
                            if (available) {
                                availableTotal += available;
                            }
                            if (backOrder) {
                                backOrderTotal += backOrder;
                            }
                            if (inTransit) {
                                inTransitTotal += inTransit;
                            }
                        });

                        whField.innerHTML = '';
                        let totalOption = document.createElement('option');
                        totalOption.innerText = `All`;
                        totalOption.setAttribute('data-available', availableTotal);
                        totalOption.setAttribute('data-backorder', backOrderTotal);
                        totalOption.setAttribute('data-intransit', inTransitTotal);
                        availableField.value = availableTotal;
                        backOrderField.value = backOrderTotal;
                        inTransitField.value = inTransitTotal;
                        whField.appendChild(totalOption);
                        inventory.map(({available, backOrder, inTransit, warehouseCd}) => {
                            let optionDom = document.createElement('option');
                            optionDom.innerText = `${warehouseCd}`;
                            optionDom.setAttribute('data-available', available);
                            optionDom.setAttribute('data-intransit', inTransit);
                            optionDom.setAttribute('data-backorder', backOrder);
                            whField.appendChild(optionDom);
                        })
                        whField.addEventListener('change', (event) => {
                            console.log(event.target.options[event.target.selectedIndex].dataset);
                            let {
                                available,
                                intransit,
                                backorder
                            } = event.target.options[event.target.selectedIndex].dataset;
                            availableField.value = available;
                            backOrderField.value = intransit;
                            inTransitField.value = backorder;
                        });
                    }
                }
            }
        })

        liEl.appendChild(productInfoContainer);
        list.appendChild(liEl);


    });

    if(!isChange && !isSalesOrder) {
        searchSelectEl.innerHTML = '';
        let selectOptions = ['All', 'Included', ...Object.keys(subCategories)];
        selectOptions.map(subCat => {
            let subOption = document.createElement('option');
            subOption.innerText = subCat;
            searchSelectEl.appendChild(subOption);
        })
    }
}

function deleteEntity(id) {
    let updatePayload = {
        entity: config.lines.name,
        id: id,
        field_values: { deleted: true }
    }
    FAClient.updateEntity(updatePayload, (data) => {
        updateRecord();
    });
}


function updateQty({id, qty=1, price='', co='', wh='', type=true, alternativeTo, typeText, competitorValue, specialDescription}) {
    let updatePayload = {
        entity: config.lines.name,
        id: id,
        field_values: {
            [config.lines.fields.quantity]: qty,
            [config.lines.fields.price]: price,
            [config.lines.fields.commissionPercentOverwrite]: co,
            [config.lines.fields.wareHouse]: wh,
            [config.lines.fields.itemType]: type,
            [config.lines.fields.descriptionSpecial]: specialDescription,
            [config.lines.fields.alternativeTo]: alternativeTo,
            [config.lines.fields.typeText]: typeText,
            [config.lines.fields.competitorName]: competitorValue,
        }
    }
    FAClient.updateEntity(updatePayload, (data) => {
        updateRecord();
    })
}

function addItem({productId, qty=1, price='', co='', wh='', type=true, alternativeTo, typeText, competitorValue, specialDescription=''}) {
    let payload = {
        entity: config.lines.name,
        field_values: {
            [config.lines.fields.itemNumber] : productId,
            [config.lines.fields.quantity]: qty,
            [config.lines.fields.price]: price,
            [config.lines.fields.commissionPercentOverwrite]: co,
            [config.lines.fields.wareHouse]: wh,
            [config.lines.fields.itemType]: type,
            [config.lines.fields.descriptionSpecial]: specialDescription,
            [config.lines.fields.alternativeTo]: alternativeTo,
            [config.lines.fields.typeText]: typeText,
            [config.lines.fields.competitorName]: competitorValue,
            parent_entity_reference_id: ""
        }
    };
    payload.field_values.parent_entity_reference_id = recordId;
    console.log({addItemPayload: payload})
    FAClient.createEntity(payload, (data) => {
        updateRecord();
        linesGlobal.push(data.entity_value)
    })
}

function updateRecord(id=recordId) {
    let updatePayload = {
        entity: config.parent.name,
        id: id,
        field_values: {}
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
    })
}


function getFilteredProductList(productsToFilter, lines, value) {
    let linesObject = {};

    let includedProductsInOrder = [];

    if(value === 'Included') {
        lines.map(line => {
            let productFound = productsToFilter.find(prod => prod.id === line.field_values.quote_item_field0.value);
            if(productFound) {
                includedProductsInOrder.push(productFound);
            }
        })
        return includedProductsInOrder;
    } else {
        if(value && value !== 'All') {
            return searchList(productsToFilter, value)
        } else {
            return productList;
        }
    }

    return productsToFilter;
}

function searchList(listToSearch, value) {
    value = value?.toLowerCase();
    return listToSearch.filter(prod => {
        let nameMatch = prod?.field_values[config.products.fields.descriptionName]?.display_value?.toLowerCase().includes(value) || null;
        let itemNumber = prod?.field_values[config.products.fields.itemNumber]?.display_value?.toLowerCase().includes(value) || null;
        let subCategory = prod?.field_values[config.products.fields.subCategory]?.display_value?.toLowerCase().includes(value) || null;
        let mainCategory = prod?.field_values[config.products.fields.mainCategory]?.display_value?.toLowerCase().includes(value) || null;
        return nameMatch || itemNumber || subCategory || mainCategory;
    })
}

function addAlternateToLines(alternateSelection, product, alternateTo=null) {
    alternateSelection.innerHTML = '';
    linesGlobal.map(line => {
        let lineItemProdId = line?.field_values[config.lines.fields.itemNumber]?.value;
        let lineItemNum = line?.field_values[config.lines.fields.itemNumber]?.display_value;
        let lineItemDesc = line?.field_values[config.lines.fields.productName]?.display_value;
        if(lineItemProdId !== product.id) {
            let lineNameOption = document.createElement('option');
            lineNameOption.innerText = `(${lineItemNum}) ${lineItemDesc}`;
            lineNameOption.setAttribute('data-productid',lineItemProdId);
            lineNameOption.setAttribute('data-lineid',line.id);
            lineNameOption.setAttribute('data-itemnum',lineItemNum);
            lineNameOption.setAttribute('data-itemdesc',lineItemDesc);
            if(alternateTo === lineItemProdId) {
                lineNameOption.setAttribute('selected', true);
            }
            alternateSelection.appendChild(lineNameOption);
        }
    });
}

function createSo() {
    document.getElementById('create-so-loading').style.display = "inline-block";
    document.getElementById('create-so-text').style.display = "none";
    FAClient.createEntity({
        entity: "sales_orders",
        field_values: {
            description: "SO Release",
            sales_orders_field7: recordId,
            sales_orders_field17: "e10ca7d4-c93d-4618-9d3c-113fa46b7181"
        }
    }, (soRecord) => {
        console.log({soRecord})
        let orderListEl = document.querySelector('#order-list');
        let allLiEl = orderListEl.querySelectorAll('li');
        allLiEl.forEach(liEl => {
            if (liEl.classList.contains('included-in-lines')) {
                let liProductId = liEl.dataset.id;
                let index = liEl.dataset.index;
                let lineId = liEl.dataset.lineid;
                let qtyInput = orderListEl.querySelector(`#qty${index}`);
                let priceInput = orderListEl.querySelector(`#price${index}`);
                let commissionInput = orderListEl.querySelector(`#co${index}`);
                let itemTypeInput = orderListEl.querySelector(`#itemType${index}`);
                let releaseAmount = priceInput?.valueAsNumber * qtyInput?.valueAsNumber;
                  FAClient.createEntity({
                      entity: "item_description",
                      field_values: {
                          item_description_field2: priceInput?.valueAsNumber,
                          item_description_field3: liProductId,
                          item_description_field5: qtyInput?.valueAsNumber,
                          item_description_field6: commissionInput?.valueAsNumber,
                          item_description_field7: itemTypeInput?.value === "Primary",
                          parent_entity_reference_id: soRecord.entity_value.id
                      }
                  }, (lineRes) => {
                      console.log(lineRes);
                  })
                if (lineId) {
                    let updatePayload = {
                        entity: config.lines.name,
                        id: lineId,
                        field_values: {
                            [config.lines.fields.releaseAmount]: releaseAmount,
                        }
                    }
                    FAClient.updateEntity(updatePayload, (data) => {
                        cosnole.log(data)
                    })
                }

            }
        })
        FAClient.createEntity({
            entity: config.poLines.name,
            field_values: {
                [config.poLines.fields.soRecordRef] : soRecord.entity_value.id,
                parent_entity_reference_id: recordId
            }
        }, (res) => {
            console.log({res})
            FAClient.updateEntity({
                entity: "sales_orders",
                id: soRecord.entity_value.id,
                field_values: {}
            }, ({entity_value}) => {
                console.log(res)
                FAClient.showModal('entityFormModal', {
                    entity: "sales_orders",
                    entityLabel: 'Sales Order',
                    entityInstance: entity_value,
                    showButtons: false,
                });
                document.getElementById('create-so-text').style.display = "flex";
                document.getElementById('create-so-loading').style.display = "none";
                generateList({products: []})
                updateRecord();
            })
        })
    })
}





function processLines(productInfoContainer, productDescription,product, index, operation) {
    let qtyInput = productInfoContainer.querySelector(`#qty${index}`);
    let priceInput = productInfoContainer.querySelector(`#price${index}`);
    let commisionInput = productInfoContainer.querySelector(`#co${index}`);
    let whField = productInfoContainer.querySelector(`#wh${index}`);
    let typeField = productInfoContainer.querySelector(`#itemType${index}`);
    let specialDescriptionField = productInfoContainer.querySelector(`#descroiptionText${index}`);
    let alternateSelection = productInfoContainer.querySelector(`.alternate-select`);
    let typeTextField = productInfoContainer.querySelector(`#typeText${index}`);
    let competitorField = productInfoContainer.querySelector(`#competitor${index}`);
    let qty = qtyInput.valueAsNumber;
    let price = priceInput.valueAsNumber;
    let isValid = false;
    if(operation === 'add') {
        if(price && price > 0 && qty && qty > 0) {
            isValid = true;
        } else {
            if(!price || price <= 0 || price === '') {
                priceInput.style.backgroundColor = 'rgba(242, 23, 22, 0.8)';
                setTimeout(() => { priceInput.style.backgroundColor = 'rgba(255,255,255,1)' }, 1000);
            }
            if(!qty || qty <= 0 || qty === '') {
                qtyInput.style.backgroundColor = 'rgba(242, 23, 22, 0.8)';
                setTimeout(() => { qtyInput.style.backgroundColor = 'rgba(255,255,255,1)' }, 1000);
            }
        }
        if(isValid) {
            let co = commisionInput.valueAsNumber;
            let wh = whField.value;
            let isPrimary = !!typeField && typeField.value === 'Primary';
            let alternativeTo = !isPrimary ? alternateSelection?.options[alternateSelection.selectedIndex]?.dataset?.productid : '';
            let typeText = typeTextField.value;
            let specialDescription = specialDescriptionField.value !== productDescription ? specialDescriptionField.value : '';
            let competitorValue = competitorField?.value ? competitorField?.value : '';

            let lineFound = linesGlobal.find(line => line.field_values[config.lines.fields.itemNumber].value === product.id);
            if (lineFound) {
                if (qty > 0) {
                    if (price && price !== '' && price > 0) {
                        updateQty({ id: lineFound.id, qty, price, co, wh, type: isPrimary, alternativeTo, typeText , competitorValue, specialDescription});
                        qtyInput.dataset.prev = qty;
                        updateRecord();
                    }
                } else {
                    deleteEntity(lineFound.id)
                    linesGlobal = linesGlobal.filter(line => line.field_values[config.lines.fields.itemNumber].value !== product.id);
                    lineFound = null;
                    document.querySelector(`#liEl${index}`).classList.remove('included-in-lines');
                    qtyInput.dataset.prev = qty;
                    updateRecord();
                }
            } else {
                if (qty > 0) {
                    let specialDescription = specialDescriptionField.value !== productDescription ? specialDescriptionField.value : '';
                    addItem({productId: product.id, qty, price, co, wh, type: isPrimary, alternativeTo,typeText,competitorValue, specialDescription});
                    document.querySelector(`#liEl${index}`).classList.add('included-in-lines')
                    qtyInput.dataset.prev = qty;
                    updateRecord();
                }
            }
        }
    }

    if(operation === 'update' || operation === 'remove') {
            let co = commisionInput.valueAsNumber;
            let wh = whField.value;
            let isPrimary = !!typeField && typeField.value === 'Primary';
            let alternativeTo = !isPrimary ? alternateSelection?.options[alternateSelection.selectedIndex]?.dataset?.productid : '';
            let typeText = typeTextField.value;
            let specialDescription = specialDescriptionField.value !== productDescription ? specialDescriptionField.value : '';
            let competitorValue = competitorField?.value ? competitorField?.value : '';
            let lineFound = linesGlobal.find(line => line.field_values[config.lines.fields.itemNumber].value === product.id);
            if (lineFound) {
                if (qty > 0 && operation !== 'remove') {
                    if (price && price !== '' && price > 0) {
                        updateQty({ id: lineFound.id, qty, price, co, wh, type: isPrimary, alternativeTo, typeText , competitorValue, specialDescription});
                        qtyInput.dataset.prev = qty;
                        updateRecord();
                    }
                }
                if(qty === 0 || operation === 'remove') {
                    deleteEntity(lineFound.id)
                    updateRecord();
                    linesGlobal = linesGlobal.filter(line => line.field_values[config.lines.fields.itemNumber].value !== product.id);
                    lineFound = null;
                    let liElement = document.querySelector(`#liEl${index}`);
                    if (document.getElementById('search-select')?.value === 'Included') {
                        liElement.remove();
                    } else {
                        liElement.classList.remove('included-in-lines');
                        qtyInput.dataset.prev = '0';
                        priceInput.value = 0;
                        qtyInput.value = 0;
                        typeTextField.value = '';
                        alternateSelection.value = 'Primary';
                    }
                }
            }
        }

}

function removeLine(productInfoContainer, productDescription,product, index) {
    let qtyInput = productInfoContainer.querySelector(`#qty${index}`);
    let priceInput = productInfoContainer.querySelector(`#price${index}`);
    let alternateSelection = productInfoContainer.querySelector(`.alternate-select`);
    let typeTextField = productInfoContainer.querySelector(`#typeText${index}`);
    let competitorField = productInfoContainer.querySelector(`#competitor${index}`);
    let lineFound = linesGlobal.find(line => line.field_values[config.lines.fields.itemNumber].value === product.id);
    if (lineFound) {
        deleteEntity(lineFound.id);
        updateRecord();
        linesGlobal = linesGlobal.filter(line => line.field_values[config.lines.fields.itemNumber].value !== product.id);
        lineFound = null;
        let liElement = document.querySelector(`#liEl${index}`);
        if (document.getElementById('search-select')?.value === 'Included') {
            liElement.remove();
        } else {
            liElement.classList.remove('included-in-lines');
            qtyInput.dataset.prev = '0';
            priceInput.value = 0;
            qtyInput.value = 0;
            typeTextField.value = '';
            alternateSelection.value = 'Primary';
        }
   }
}

function renderedReorderLines(lines) {
    let orderList = document.querySelector('#order-list');
    let innerHTML = ``;
    lines.map(({field_values, id}, index) => {
        let num = field_values?.quote_item_field0?.display_value;
        let description = field_values?.quote_item_field22?.display_value;
        innerHTML += `<li class="reorder-element" style="color: white"><b style="white-space: pre-line"><span style="color: rgba(215, 217, 219, 1.00)"> (${num})</span> ${description} <span><img src="height_white_18dp.svg" alt=""/></span></b></li>`;
    })
    orderList.innerHTML = innerHTML;
    $( "#order-list" ).sortable({
        change: function( event, ui ) {
            console.log({event, ui})
        }
    });
    $( "#order-list" ).disableSelection();
    return null;
}


async function postData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    return response.json();
}

let baseUrl = "https://developer.maxlite.com/api/crm/items";

let headers = {
    "MX-Api-Key" : "",
    "accept": "text/plain"
}

async function getItem(itemNo) {
    headers['MX-Api-Key'] = keyGlobal;
    let url = `${baseUrl}?itemNo=${itemNo}`
    let response = await fetch(url, {
        method: 'GET',
        headers: headers,
    });
    if (response.ok) {
        return response.json();
    }
    return null;
}

async function getCommission(itemNo, unitPrice=50.5, qty= 100, repNo=122) {
    let url = `${baseUrl}/commission?itemNo=${itemNo}&unitPrice=${unitPrice}&qty=${qty}&repNo=${repNo}`;
    headers['MX-Api-Key'] = keyGlobal;
    let response =  await fetch(url, {
        method: 'GET',
        headers,
    });
    if (response && response.ok) {
        return response.json();
    }
    return null;
}
